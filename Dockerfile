# Build remco from specific commit
######################################################
FROM golang AS remco

# remco (lightweight configuration management tool) https://github.com/HeavyHorst/remco
RUN go install github.com/HeavyHorst/remco/cmd/remco@latest


# Build soulmask base
######################################################
FROM ubuntu:oracular AS soulmask-base
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ENV DEBIAN_FRONTEND noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV SOULMASK_HOME=/home/soulmask
ENV SOULMASK_UID=10000
ENV SOULMASK_GID=10000

USER root

## Install system packages
RUN dpkg --add-architecture i386
RUN apt-get -y update && apt-get -y upgrade && apt-get -y --no-install-recommends install \
    curl \
    vim \
    ca-certificates \
    file \
    lib32gcc-s1

## Create soulmask user and group
RUN groupadd -g $SOULMASK_GID soulmask && \
    useradd -l -s /bin/bash -d $SOULMASK_HOME -m -u $SOULMASK_UID -g soulmask soulmask && \
    passwd -d soulmask
# RUN echo "soulmask ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/soulmask

## Install remco
COPY --from=remco /go/bin/remco /usr/local/bin/remco
COPY --chown=soulmask:root remco /etc/remco
RUN chmod -R 0775 /etc/remco

RUN curl -Lo /usr/local/bin/yq https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 && \
    chmod guo+x /usr/local/bin/yq

## Update permissions
COPY --chown=soulmask:soulmask files/entrypoint.sh ${SOULMASK_HOME}/
RUN chmod ugo+x ${SOULMASK_HOME}/entrypoint.sh && \
    chown -Rv soulmask:soulmask ${SOULMASK_HOME}/


# Build soulmask image
######################################################
FROM soulmask-base as soulmask

ENV DEBIAN_FRONTEND noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV SOULMASK_HOME=/home/soulmask
ENV SOULMASK_STEAM_VALIDATE="false"
ENV REMCO_HOME=/etc/remco
ENV STEAMAPPID=3017300

ARG CI_COMMIT_AUTHOR
ARG CI_COMMIT_TIMESTAMP
ARG CI_COMMIT_SHA
ARG CI_COMMIT_TAG
ARG CI_PROJECT_URL

LABEL maintainer=$CI_COMMIT_AUTHOR
LABEL author=nathan.snow@mimir-tech.org
LABEL description="Soulmask dedicated server with SteamCMD automatic updates and remco auto-config"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name="registry.gitlab.com/japtain_cack/soulmask-server"
LABEL org.label-schema.description="Soulmask dedicated server with SteamCMD automatic updates and remco auto-config"
LABEL org.label-schema.url=$CI_PROJECT_URL
LABEL org.label-schema.vcs-url=$CI_PROJECT_URL
LABEL org.label-schema.docker.cmd="docker run -it -d -v /mnt/soulmask/world1/:/home/soulmask/server/ -p 7777/udp -p 27015/udp -p 18888/tcp registry.gitlab.com/japtain_cack/soulmask-server"
LABEL org.label-schema.vcs-ref=$CI_COMMIT_SHA
LABEL org.label-schema.version=$CI_COMMIT_TAG
LABEL org.label-schema.build-date=$CI_COMMIT_TIMESTAMP

WORKDIR ${SOULMASK_HOME}
VOLUME "${SOULMASK_HOME}/server"
# server ports
EXPOSE 8777/udp
EXPOSE 27015/udp
EXPOSE 18888/tcp

copy --chown=soulmask:soulmask files/GameXishu.json .

USER soulmask
ENTRYPOINT ["./entrypoint.sh"]

