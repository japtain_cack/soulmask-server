[![Tag](https://img.shields.io/gitlab/v/tag/japtain_cack/soulmask-server?style=for-the-badge)](https://gitlab.com/japtain_cack/soulmask-server/-/tags)
[![Gitlab Pipeline](https://img.shields.io/gitlab/pipeline-status/japtain_cack/soulmask-server?branch=master&style=for-the-badge)](https://gitlab.com/japtain_cack/soulmask-server/-/pipelines)
[![Issues](https://img.shields.io/gitlab/issues/open/japtain_cack/soulmask-server?style=for-the-badge)](https://gitlab.com/japtain_cack/soulmask-server/-/issues)
[![License](https://img.shields.io/badge/License-CC%20BY--ND%204.0-blue?style=for-the-badge)](https://creativecommons.org/licenses/by-nd/4.0/)

# soulmask-server
Run a Soulmask dedicated server in a Docker container.

This uses steamCMD to automatically update your server software.

This Dockerfile will download the Soulmask dedicated server app and set it up, along with its dependencies.

If you run the container as is, the `game` directory will be created inside the container, which is inadvisable.
It is highly recommended that you store your game files outside the container using a mount (see the example below).
Ensure that your file system permissions are correct, `chown 10000:10000 mount/path`, and/or modify the UID/GID variables as needed (see below).

It is also likely that you will want to customize your `start.sh` file.
To do this, use the `-e <ENVIRONMENT_VARIABLE>=<value>` for each setting in the `start.sh` file.
The `start.sh` file will be overwritten every time the container is launched. See below for details.


# Run the server
## Docker
Use this `docker run` command to launch a container with a few customized `start.sh` options.
Replace `<ENVIRONMENT_VARIABLE>=<VALUE>` with the appropriate values (see section "Server properties and environment variables" below).

```
docker run --name soulmask -it --rm \
  -p 8777:8777/udp \
  -p 27015:27015/udp \
  -p 18888:18888/tcp \
  -v /home/$(whoami)/soulmask:/home/soulmask/server \
  -e SOULMASK_SERVERNAME="soulmask.example.com" \
  -e SOULMASK_STEAM_VALIDATE="false" \
  -e SOULMASK_PVE="false" \
  -e SOULMASK_ADMIN_PASSWORD='Secret123!' \
  #-e SOULMASK_<some_key>="some value" \
  registry.gitlab.com/japtain_cack/soulmask-server:latest
```

## Versioning
The CI/CD pipeline will generate tags using `major.minor.patch` syntax. Example `1.0.0`. You can use just
the major version `1`, the major and minor version `1.0`, or the full version `1.0.0` when setting the
docker image tag.

It is recommended that you at least pin to the major version `1` instead of using latest. Major versions
typically include breaking changes.

### Set selinux context for mounted volumes

`chcon -Rt svirt_sandbox_file_t /path/to/volume`

### Additional Docker commands

**kill and remove all docker containers**

`docker kill $(docker ps -qa); docker rm $(docker ps -qa)`

**docker logs**

`docker logs -f soulmask`

**exec into the container's bash console**

`docker exec soulmask bash`

**NOTE**: referencing containers by name is only possible if you specify the `--name` flag in your docker run command.

## Kustomize
* `git clone https://gitlab.com/japtain_cack/soulmask-server.git`
* `kubectl apply -k soulmask-server`

### NOTICE
If the pods are restarted multiple times during the initial download process or during updates, then
adjust the `initialDelaySeconds` in the statefulSet.

# Configuration
## Reference files
* [start.sh](https://gitlab.com/japtain_cack/soulmask-server/-/blob/master/remco/templates/start.sh)
  * Configs in the start.sh use the `SOULMASK_<key>` format.
* [GameXishu.json](https://gitlab.com/japtain_cack/soulmask-server/-/blob/master/remco/templates/GameXishu.json)
  * Configs in the Game.ini use the `SOULMASK_<key>` format.
* [Helm values.yaml](https://gitlab.com/japtain_cack/soulmask-server/-/blob/master/helm_chart/values.yaml)

## Faster startup, no steamCMD validation
SteamCMD validation is disabled by default, it was causing the server to take a lot more time on boot.
Soulmask already takes FOREVER to boot, so this is disabled by default.

To enable SteamCMD file validation, in case you need to re-validate your pod/container data. This can
be enabled on demand by adding an environment variable `SOULMASK_STEAM_VALIDATE=true` to your helm values or docker run
command.
 
## Environment Variables
This project uses [Remco config management](https://github.com/HeavyHorst/remco)
This allows for templatization of config files and options can be set using environment variables.
This allows for easier deployments using most docker orchistration/management platforms including Kubernetes.

### Set user and/or group id (optional)
* `SOULMASK_UID=10000`
* `SOULMASK_GID=10000`

### remco environment variables
Let's start by looking at `start.sh`, you can also look at `Game.ini`. You should see something like this
`-ip="{{ getv("/soulmask/servername", "World1") }}"`. `start.sh` uses keys to determine the format of the
environment variable. Let's break this down.

* `-servername` is simply static text, in this example we are setting the `-ServerName` CLI option.
* `{{ ... }}`. Everything in the brackets will be rendered using the templating language processor. 
* `getv("/key", "default_value")` is a function that takes two arguments. This looks up the environment variable based on the
  key, `"/soulmask/servername"`, and sets the value. If there is no value set in the env, the default is used.
* `"/soulmask/server/ip"` this is a key, which maps to an environment variable. Environment variables should be all CAPS
  and separated by underscores. Basically, slashes `/` become underscores `_` and everything is capatalized.
  `"/soulmask/servername"` becomes `SOULMASK_SERVERNAME`. Simple.

You can see an example of some of the environment variables in the helm chart `values.yaml`.

